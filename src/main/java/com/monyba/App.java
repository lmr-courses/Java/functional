package com.monyba;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * The Main Class
 */
public class App {
    public static void main( String[] args ) {
        Optional<List<String>> argsList = Optional.of(Arrays.asList(args));
        argsList.ifPresentOrElse(list -> list.forEach(System.out::println),
                () -> System.out.println("Hello World!"));
    }
}
