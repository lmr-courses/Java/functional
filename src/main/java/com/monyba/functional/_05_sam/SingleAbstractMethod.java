package com.monyba.functional._05_sam;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Function;

/**
 * Se le considera SAM a una interface cuando tiene un unico metodo, sin valor definido.
 * Este concepto es importante empezando en Java 8 por varias razones, la principal
 * es que el compilador nos permitirá usar una anotacion disponible UNICAMENTE para
 * SAMs: @FunctionalInterface
 */
public class SingleAbstractMethod {
  public static void main(String[] args) {
    Function<Integer, String> addCeros = x -> x < 10 ? "0" + x : String.valueOf(x);

    TriFunction<Integer, Integer, Integer, LocalDate> parseDate =
            (day, month, year) -> LocalDate.parse(year + "-" + addCeros.apply(month) + "-" + addCeros.apply(day));

    TriFunction<Integer, Integer, Integer, Integer> calculateAge =
            (day, month, year) -> Period.between(parseDate.apply(day, month, year), LocalDate.now()).getYears();

    System.out.println(calculateAge.apply(10, 10, 1985));
  }

  @FunctionalInterface
  interface TriFunction<T, U, V, R> {
    R apply(T t, U u, V v);
  }

}
