package com.monyba.functional._01_pure;

import java.util.Random;

/**
 * Las Funciones Puras son aquellas que cumplen con dos requisitos básicos
 * - Dado unos parámetros de entrada de idéntico valor, la función siempre
 *   devolverá el mismo resultado.
 * - Una función pura no depende de estados exteriores (propiedades, objetos, variables
 *   externas a su definición, etc.) ni ve afectado su resultado por agentes externos.
 */
public class PureFunctions {
  /**
   * Esta Función es pura porque siempre que la llamemos con los
   * mismos parámetros el resultado será el mismo.
   */
  private int pureSum(int x, int y){
    return x + y;
  }


  /**
   * Imagina que la siguiente clase es parte de un sistema financiero
   */
  static class Person {
    private double balance;

    public Person(double balance) {
      this.balance = balance;
    }

    public double getBalance() {
      return balance;
    }

    public void setBalance(double balance) {
      this.balance = balance;
    }
  }

  public static boolean hasAvailableFunds(double funds) {
    return funds > 0.0;
  }


  /**
   * La funcion es evaluada al momento y no depende de que objeto es quien la
   * manda a invocar, es por ello que se considera pura.
   */
  public static void main(String[] args) {
    Person elmer = new Person(-20.00);
    System.out.println(hasAvailableFunds(elmer.getBalance()));

    Person gabriela = new Person(1300.00);
    System.out.println(hasAvailableFunds(gabriela.getBalance()));
  }

}
