package com.monyba.functional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Streams {
  public static void main(String[] args) {
    // Normal
    List<String> courseList = Arrays.asList("Java", "Kotlin", "Javascript", "Typescript", "Python", "Golang");
    for (String course : courseList){
      String newCoursename = course.toLowerCase().replace("!", "!!");
      System.out.println("Course: " + newCoursename);
    }

    // Streams
    Stream<String> coursesStream = Stream.of("Java", "Kotlin", "Javascript", "Typescript", "Python", "Golang");
    coursesStream
            .map(x->x.toLowerCase().replace("!","!!"))
            .forEach(x-> System.out.println("Course: "+ x));
  }
}
