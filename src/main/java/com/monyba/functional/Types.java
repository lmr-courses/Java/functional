package com.monyba.functional;

import java.util.stream.IntStream;

public class Types {
  public static void main(String[] args) {
    /**
     * Usando parallel usamos todos los cores de nuestro procesador.
     * Usar esto cuando no nos importe el orden de ejecucion.
     */
    IntStream infiniteStream = IntStream.iterate(0, x -> x + 1);
    infiniteStream.limit(1000)
            .parallel()
            .filter(x->x % 2 == 0)
            .forEach(System.out::println);
  }
}
