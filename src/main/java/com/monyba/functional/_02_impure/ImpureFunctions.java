package com.monyba.functional._02_impure;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

/**
 * Las Funciones Impuras son aquellas que cumplen con dos requisitos básicos
 * - Dado unos parámetros de entrada de idéntico valor, la función siempre
 *   devolverá diferentes resultados.
 * - Una función impura depende de estados exteriores (propiedades, objetos, variables
 *   externas a su definición, etc.).
 */
public class ImpureFunctions {
  /**
   * Es impura porque devuelve valores diferentes
   * para los mismos parámetros de entrada
   */
  private int getNumberInRange(int min, int max){
    Random random = new Random();
    return random.nextInt(max - min + 1) + min;
  }

  /**
   * Es impura porque tiene una dependencia externa
   */
  private int numX;
  private int impureSum(int x, int y){
    return x + y + this.numX;
  }

  /**
   * Es impura porque depende de la existencia y el estado de un archivo,
   * eso provoca que sea no determinista.
   * ¿Qué sucede si esta función se ejecuta en mi computadora y en tu computadora?
   * - Podemos determinar la salida en ambos casos?
   * - Como nos aseguramos que nadie más esté modificando el archivo?
   */
  static boolean containsCanada(File file) {
    try (BufferedReader bfReader = new BufferedReader(new FileReader(file))) {
      String line;
      while ((line = bfReader.readLine()) != null) {
        if (line.contains("Canada")) {
          return true;
        }
      }
    } catch (IOException ignored) {
      return false;
    }

    return false;
  }

  /**
   * Es impura. Aunque el código no está implementado, con entender lo que hace
   * sabemos que es no determinista y que no podemos garantizar los resultados para
   * un cierto parámetro.
   */
  static String getLastNameForGivenName(String name) {
    //Obtener una conexion a la Base de datos
    //Ejecutar un query en la base de datos
    //Revisar los resultados del query…
    //retornar el valor del lastName o un valor por default en caso de ausencia
    //...
    return "";
  }

}
