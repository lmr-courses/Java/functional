package com.monyba.functional;

import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.List;
import java.util.function.*;

public class Functions {
  public static void main(String[] args) {
    function();
    biFunction();
    predicate();
    supplier();
    consumer();
    unaryOperator();
    binaryOperator();
    custom();
    referenceOperator();
    typeInference();
    lambdaSyntax();
    defaultMethod();
    chaining();
    composicion();
  }

  /**
   * La interfaz Function es una interfaz funcional que representa
   * una función que toma un solo parámetro y devuelve un solo valor.
   * Function <Parametro, Respuesta>
   */
  static void function(){
    Function<Integer, Integer> square = new Function<Integer, Integer>() {
      @Override
      public Integer apply(Integer x) {
        return x*x;
      }
    };
    System.out.println(square.apply(5));

    // With
    Function<Integer, Integer> square2 = x -> x * x;
    System.out.println(square2.apply(6));
  }

  /**
   * La interfaz BiFunction es una interfaz funcional que representa
   * una función que toma dos parámetros y devuelve un solo valor.
   * Function <Parametro, Parametro, Respuesta>
   */
  static void biFunction(){
    BiFunction<Integer, Integer, Integer> square = new BiFunction<Integer, Integer, Integer>() {
      @Override
      public Integer apply(Integer x, Integer y) {
        return x * y;
      }
    };
    System.out.println(square.apply(4, 5));

    BiFunction<Integer, Integer, Integer> square2 = (x, y) -> x * x;
    System.out.println(square2.apply(4, 5));
  }

  /**
   * La interfaz Predicate es una interfaz funcional que representa
   * una función simple que toma un único valor
   * como parámetro y devuelve verdadero o falso
   */
  private static void predicate() {
    Predicate<Integer> isOdd = new Predicate<Integer>() {
      @Override
      public boolean test(Integer x) {
        return x % 2 == 1;
      }
    };
    System.out.println(isOdd.test(5));

    Predicate<Integer> isEven = x -> x % 2 == 0;
    System.out.println(isEven.test(4));
  }

  /**
   * La interfaz Supplier es una interfaz funcional que representa
   * una función que proporciona un valor de algún tipo.
   * La interfaz del proveedor también se puede considerar
   * como una interfaz de fábrica.
   */
  private static void supplier() {
    Supplier<Integer> supplier = () -> Integer.valueOf((int) (Math.random() * 1000D));
    System.out.println(supplier.get());
  }

  /**
   * La interfaz Consumer es una interfaz funcional que representa
   * una función que consume un valor sin devolver ningún valor.
   * Una implementación de Java Consumer podría imprimir un valor,
   * escribirlo en un archivo, o en la red, etc.
   */
  private static void consumer() {
    Consumer<Integer> consumer = x -> System.out.println("El valor es: " + x);
    consumer.accept(200);
  }

  /**
   * La interfaz UnaryOperator es una interfaz funcional que representa
   * una operación que toma un solo parámetro y devuelve un parámetro del mismo tipo
   */
  private static void unaryOperator() {
    UnaryOperator<Integer> unaryOperator = x -> x * 5;
    System.out.println(unaryOperator.apply(5));
  }

  /**
   * La interfaz BinaryOperator es una interfaz funcional que representa
   * una operación que toma dos parámetros y devuelve un solo valor.
   * Ambos parámetros y el tipo de devolución deben ser del mismo tipo.
   * Ejemplo: suma, resta, multiplicación, division de dos números
   */
  private static void binaryOperator() {
    BinaryOperator<Integer> binaryOperator = (x, y) -> x * y;
    System.out.println(binaryOperator.apply(4,5));
  }

  /**
   * Podemos crear nuestra propia interfaz funcional
   */
  private static void custom() {
    Function<Integer, String> addCeros = x -> x < 10 ? "0" + x : String.valueOf(x);

    TriFunction<Integer, Integer, Integer, LocalDate> parseDate =
            (day, month, year) -> LocalDate.parse(year + "-" + addCeros.apply(month) + "-" + addCeros.apply(day));

    TriFunction<Integer, Integer, Integer, Integer> calculateAge =
            (day, month, year) -> Period.between(parseDate.apply(day, month, year), LocalDate.now()).getYears();

    System.out.println(calculateAge.apply(10, 10, 1985));

  }

  @FunctionalInterface
  interface TriFunction<T, U, V, R> {
    R apply(T t, U u, V v);
  }

  /**
   * Operador de Referencia de método en Java, se utiliza para llamar
   * a un método haciendo referencia a él con la ayuda de su clase directamente.
   */
  private static void referenceOperator(){
    List<String> profesores = getList("Nicolas", "Roberto", "Giancarlo");
    Consumer<String> printer = x -> System.out.println(x); // Con lambda
    profesores.forEach(printer);
    profesores.forEach(System.out::println); // Con operador de referencia
  }

  static <T> List<T> getList(T... elements){
    return Arrays.asList(elements);
  }

  /**
   * Inferencia de Tipos
   */
  private static void typeInference(){
    List<String> alumnos = Arrays.asList("Hugo", "Paco", "Luis");
    alumnos.forEach((String name) -> System.out.println(name));
    alumnos.forEach(name -> System.out.println(name));
    alumnos.forEach(System.out::println);
  }

  /**
   *
   */
  private static void lambdaSyntax(){
    List<String> cursos = Arrays.asList("Java", "Kotlin", "Javascript", "Typescript");
    cursos.forEach(curso -> System.out.println(curso));
    usarZero(()->2);
    usarPredicado(text -> text.isEmpty());
    usarBiFuncion((x,y)-> x * y);
    usarBiFuncion((x,y)-> {
      System.out.println("X:"+x+", Y:"+y);
      return x * y;
    });
    usarNada(()->{
      System.out.println("hola");
    });
  }

  static void usarZero(ZeroArguments zeroArguments){

  }

  static void usarPredicado(Predicate<String> predicado){

  }

  static void usarBiFuncion(BiFunction<Integer, Integer, Integer> operacion){

  }

  static void usarNada(OperarNada operarNada){

  }

  @FunctionalInterface
  interface ZeroArguments{
    int get();
  }

  @FunctionalInterface
  interface OperarNada{
    void nada();
  }

  /**
   *
   */
  private static void defaultMethod(){
    StringOperation six = () -> 6;
    six.operate("hola");

    DoOperation operationFive = text -> System.out.println(text);
    operationFive.execute(5,"Loco" );
  }

  @FunctionalInterface
  interface StringOperation{
    int getAmount();

    default void operate(String text){
      int x = getAmount();
      while (x-- > 0){
        System.out.println(text);
      }
    }
  }

  @FunctionalInterface
  interface DoOperation{
    void take(String text);

    default void execute(int x, String text){
      while (x-- > 0){
        take(text);
      }
    }
  }

  /**
   * Chaining
   */
  private static void chaining(){
    Chainer chainer = new Chainer();
    chainer.sayHi().sayBye();
  }

  static class Chainer{
    public Chainer sayHi(){
      System.out.println("Hola");
      return this;
    }
    public Chainer sayBye(){
      System.out.println("Adios");
      return this;
    }
  }

  /**
   * Composicion
   */
  private static void composicion(){
    Function<Integer, Integer> multiplyBy3 = x -> {
      System.out.println("Multiplicando X:"+x+" por 3");
      return x * 3;
    };

    Function<Integer, Integer> add1 = x -> {
      System.out.println("Le agregue 1 a: "+x);
      return x + 1;
    };

    Function<Integer, Integer> square = x -> {
      System.out.println("Elevando al cuadrado a: "+x);
      return x * x;
    };

    Function<Integer, Integer> add1multiplyBy3 = multiplyBy3.compose(add1);
    Function<Integer, Integer> andSquare = add1multiplyBy3.andThen(square);
    System.out.println(andSquare.apply(3));

  }

}
