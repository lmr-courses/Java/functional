package com.monyba.functional._04_functional;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class _01_Functions {

  public static void main(String[] args) {
    functionalInterfaceNormal();
    functionalInterfaceLambda();
    Function<Integer, String> a = functionalInterfaceGeneric();
    System.out.println(a.apply(333));
    Function<String, String> b = functionalInterfaceGeneric();
    System.out.println(b.apply("world"));
    FunctionBy2 fun = new FunctionBy2();
    System.out.println(fun.apply(4));
    functionsAreTypes();
    funWithFuns();
    runTransform();
  }

  /**
   * La interfaz Function es una interfaz funcional que representa
   * una función que toma un solo parámetro y devuelve un solo valor.
   * Function <Parametro, Respuesta>
   */
  static void functionalInterfaceNormal(){
    Function<Integer, Integer> square = new Function<Integer, Integer>() {
      @Override
      public Integer apply(Integer x) {
        return x*x;
      }
    };
    System.out.println(square.apply(5));
  }

  static void functionalInterfaceLambda(){
    Function<Integer, Integer> square2 = x -> x * x;
    System.out.println(square2.apply(6));
  }

  static <T> Function<T, String> functionalInterfaceGeneric(){
    return x -> "Hello "+x.toString();
  }

  static class FunctionBy2 implements Function<Integer, Integer> {
    @Override
    public Integer apply(Integer x) {
      return x * 2;
    }
  }

  private static void functionsAreTypes() {
    Function<Integer, Function<Integer, Integer>> multiply =
            x -> (Function<Integer, Integer>) y -> x * y;

    // Usando nuestra nueva funcion…
    System.out.println(multiply.apply(5).apply(4));
    System.out.println(multiply.apply(15).apply(3));

    // O podemos crear funciones derivadas de la primera función:
    Function<Integer, Integer> multiplyBy3 = multiply.apply(3);
    System.out.println(multiplyBy3.apply(2));
    System.out.println(multiplyBy3.apply(9));
  }

  private static void funWithFuns() {
    List<Integer> myNumbers = Arrays.asList(1, 2, 3, 4, 5, 6);

    Function<Integer, Integer> square = x -> x * x;
    Function<Integer, Integer> cube = x -> x * x * x;
    Function<Integer, Integer> toNegative = x -> -1 * x;

    applyMathToList(myNumbers, square); // [1, 4, 9, 16, 25, 36]
    applyMathToList(myNumbers, cube); // [1, 8, 27, 64, 125, 216]
    applyMathToList(myNumbers, toNegative); // [-1, -2, -3, -4, -5, -6]
  }

  private static List<Integer> applyMathToList(List<Integer> items, Function<Integer, Integer> operation) {
    List<Integer> resultItems = new ArrayList<>();
    for (Integer x : items) {
      resultItems.add(operation.apply(x));
    }
    return resultItems;
  }

  private static String transformText(String text) {
    List<Function<String, String>> transformations = new ArrayList<>();

    transformations.add(s -> s.toUpperCase());
    transformations.add(s -> s.replaceAll("SI", "TI"));
    transformations.add(s -> s.replaceAll("RO", "YO"));
    transformations.add(s -> s.replaceAll("O", "OoO"));

    String result = text;
    for (Function<String, String> function : transformations) {
      result = function.apply(result);
    }
    return result;
  }


  private static void runTransform() {
    System.out.println(transformText("Hello")); //HELLOoO
    System.out.println(transformText("World")); //WOoORLD
    System.out.println(transformText("Claro que si roncas")); //CLAYOoO QUE TI YOoONCAS
  }
}
