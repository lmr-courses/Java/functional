package com.monyba.functional._04_functional;

import java.util.function.*;

public class _02_Functions {
  public static void main(String[] args) {
    biFunction();
    predicate();
    supplier();
    consumer();
    unaryOperator();
    binaryOperator();
  }

  /**
   * La interfaz BiFunction es una interfaz funcional que representa
   * una función que toma dos parámetros y devuelve un solo valor.
   * Function <Parametro, Parametro, Respuesta>
   */
  static void biFunction(){
    BiFunction<Integer, Integer, Integer> square = new BiFunction<Integer, Integer, Integer>() {
      @Override
      public Integer apply(Integer x, Integer y) {
        return x * y;
      }
    };
    System.out.println(square.apply(4, 5));

    BiFunction<Integer, Integer, Integer> square2 = (x, y) -> x * x;
    System.out.println(square2.apply(4, 5));
  }

  /**
   * La interfaz Predicate es una interfaz funcional que representa
   * una función simple que toma un único valor
   * como parámetro y devuelve verdadero o falso
   */
  private static void predicate() {
    Predicate<Integer> isOdd = new Predicate<Integer>() {
      @Override
      public boolean test(Integer x) {
        return x % 2 == 1;
      }
    };
    System.out.println(isOdd.test(5));

    Predicate<Integer> isEven = x -> x % 2 == 0;
    System.out.println(isEven.test(4));
  }

  /**
   * La interfaz Supplier es una interfaz funcional que representa
   * una función que proporciona un valor de algún tipo.
   * La interfaz del proveedor también se puede considerar
   * como una interfaz de fábrica.
   */
  private static void supplier() {
    Supplier<Integer> supplier = () -> Integer.valueOf((int) (Math.random() * 1000D));
    System.out.println(supplier.get());
  }

  /**
   * La interfaz Consumer es una interfaz funcional que representa
   * una función que consume un valor sin devolver ningún valor.
   * Una implementación de Java Consumer podría imprimir un valor,
   * escribirlo en un archivo, o en la red, etc.
   */
  private static void consumer() {
    Consumer<Integer> consumer = x -> System.out.println("El valor es: " + x);
    consumer.accept(200);
  }

  /**
   * La interfaz UnaryOperator es una interfaz funcional que representa
   * una operación que toma un solo parámetro y devuelve un parámetro del mismo tipo
   */
  private static void unaryOperator() {
    UnaryOperator<Integer> unaryOperator = x -> x * 5;
    System.out.println(unaryOperator.apply(5));
  }

  /**
   * La interfaz BinaryOperator es una interfaz funcional que representa
   * una operación que toma dos parámetros y devuelve un solo valor.
   * Ambos parámetros y el tipo de devolución deben ser del mismo tipo.
   * Ejemplo: suma, resta, multiplicación, division de dos números
   */
  private static void binaryOperator() {
    BinaryOperator<Integer> binaryOperator = (x, y) -> x * y;
    System.out.println(binaryOperator.apply(4,5));
  }
}
