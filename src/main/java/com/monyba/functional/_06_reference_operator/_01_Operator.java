package com.monyba.functional._06_reference_operator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * :: -> A esos 4 puntos se le llama Operator
 */
public class _01_Operator {
  /**
   * Un pequeño ejemplo de como el operador :: funciona
   */
  public static void main(String[] args) {
    List<String> names = new ArrayList<>();
    names.add("Fer");
    names.add("Orly");
    names.add("Sier");
    names.add("Chris");
    names.add("Eryx");

    //Procesamos en una lambda
    names.forEach(s -> System.out.println(s));

    // forEach toma un Consumer<T> que son funciones que toman un elemento y no retornan un resultado
    // System.out.println(Object obj) es un metodo que toma un elemento y no tiene retorno
    names.forEach(System.out::println);

    // Toda funcion que cumpla con el tipo de entrada y sin retornar valor pouede usarse
    names.forEach(_01_Operator::coolStuffWithAString);
  }

  private static void coolStuffWithAString(String str) {
    System.out.println(
            str.toUpperCase()
                    .trim()
                    .replaceAll("S", "Z")
                    .replaceAll("i", "i1Ii")
    );
  }

  /**
   * Hasta ahora hemos usado unicamente funciones estaticas, pero existen algunos casos
   * en los que quisiéramos usar un método de un objeto… porque tal vez el objeto ya
   * tiene un valor para operar, tiene alguna propiedad que puede ayudarnos en la operacion,
   * metodos que puedan ayudar a procesar los datos, etc.
   */
  private void weirdStuff() {
    //Podemos usar directamente el paso de una funcion
    giveMeAFunction(stringParam -> stringParam.length());
    giveMeAFunction(String::length);

    //Otro ejemplo de ello, es si nosotros tenemos un objeto totalmente ajeno
    //a las operaciones, pero este objeto tiene un metodo cuya definicion coincide
    //con la definicion de la funcion que necesitamos:
    HelperOperator sier = new HelperOperator("Sier");
    giveMeAFunction(sier::sayNameAge);
  }

  private static void giveMeAFunction(Function<String, Integer> function) {
    function.apply("Hello");
  }

  private class HelperOperator {
    private String name;

    public HelperOperator(String name) {
      this.name = name;
    }

    public int sayNameAge(String s) {
      System.out.println("My name is " + name);
      return s.length();
    }
  }

  private static void howItWorks(){
    List<String> names = Arrays.asList("Fer", "Orly", "Sinuhe", "Ana");
    Consumer<String> printer = _01_Operator::coolStuffWithAString;

    //Esto nos puede reducir las definiciones
    Consumer<String> outPrinter = s -> System.out.println(s);
    //Exactamente lo mismo que la linea de arriba
    Consumer<String> systemPrinter = System.out::println;

    //Todas las invocaciones son valid
    names.forEach(printer);
    names.forEach(outPrinter);
    names.forEach(systemPrinter);
  }
}
