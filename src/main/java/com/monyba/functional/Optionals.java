package com.monyba.functional;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Optionals {
  public static void main(String[] args) {
    List<String> names = getNames();
    if (names != null){

    }

    Optional<List<String>> optionalNames = getOptinalNames();
    if(optionalNames.isPresent()){

    }

    optionalNames.ifPresent(on -> on.forEach(System.out::println));

    String valuablePlayerName = optionalValuablePlayer().orElseGet(()->"No Player");

  }

  static List<String> getNames(){
    List<String> list = new LinkedList<>();
    return Collections.emptyList();
  }

  static String mostValuablePlayer(){
    return null;
  }

  static int mostExpensiveItem(){
    return -1;
  }

  static Optional<List<String>> getOptinalNames(){
    List<String> namesList = new LinkedList<>();
    return Optional.of(namesList);
  }

  static Optional<String> optionalValuablePlayer(){
    //return Optional.ofNullable();
    try {
      return Optional.of("Elmer");
    } catch (Exception e){
      e.printStackTrace();
    }
    return Optional.empty();
  }

}
