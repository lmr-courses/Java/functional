package com.monyba.functional._03_immutability.mutable;

import java.util.List;

/**
 * POJO comun. Incluye propiedades y metodos para acceder y modificar dichas propiedades
 */
public class MutablePerson1 {
  private String name;
  private List<String> emails;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getEmails() {
    return emails;
  }

  public void setEmails(List<String> emails) {
    this.emails = emails;
  }

  @Override
  public String toString() {
    return "MutablePerson1{" +
            "name='" + name + '\'' +
            ", emails=" + emails +
            '}';
  }
}
