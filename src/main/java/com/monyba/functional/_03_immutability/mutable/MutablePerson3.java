package com.monyba.functional._03_immutability.mutable;

import java.util.List;

/**
 * Mas mejoras. Ahora nuestra lista de emails es final.
 * Final evita que asigne un nuevo valor a la variable, pero no
 * evita que su contenido pueda ser editable.
 */
public class MutablePerson3 {
  private String name;
  private final List<String> emails;

  public MutablePerson3(String name, List<String> emails) {
    this.name = name;
    this.emails = emails;
  }

  public String getName() {
    return name;
  }

  public List<String> getEmails() {
    return emails;
  }

  @Override
  public String toString() {
    return "MutablePerson3{" +
            "name='" + name + '\'' +
            ", emails=" + emails +
            '}';
  }
}
