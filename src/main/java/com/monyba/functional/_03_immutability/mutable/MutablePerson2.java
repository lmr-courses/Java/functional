package com.monyba.functional._03_immutability.mutable;

import java.util.List;

/**
 * Clase mejorada.
 * Ahora obligamos a quien use esta clase a crear instancias usando el constructor.
 * Quitamos el setter para evitar que hagan modificaciones peligrosas…
 */
public class MutablePerson2 {
  private String name;
  private List<String> emails;

  public MutablePerson2(String name, List<String> emails) {
    this.name = name;
    this.emails = emails;
  }

  public String getName() {
    return name;
  }

  public List<String> getEmails() {
    return emails;
  }

  @Override
  public String toString() {
    return "MutablePerson2{" +
            "name='" + name + '\'' +
            ", emails=" + emails +
            '}';
  }
}
