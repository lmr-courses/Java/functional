package com.monyba.functional._03_immutability.mutable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Alguien decide que puede extender de nuestra clase y cambiar solo algunos
 * elementos… ¡nos hackea!
 *
 * De nada sirvieron las modificaciones en la clase MutablePerson3, esta clase nos
 * esta suplantando!
 */
public class MutablePerson4 extends MutablePerson3{
  public MutablePerson4(String name, List<String> emails) {
    super(name, emails);
  }

  @Override
  public List<String> getEmails() {
    List<String> spammyEmails = Arrays.asList("tubanco@mibanco.banco.com","cheapfoods@blackmarket.com");
    return spammyEmails;
  }

}
