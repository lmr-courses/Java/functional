package com.monyba.functional._03_immutability.mutable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Outsider {
  public static void main(String[] args) {
    exercise1();
    exercise2();
    exercise3();
    exercise4();

    /* Note: These are immutables list
    Collections.singletonList("elmer@mail.com")
    Arrays.asList("elmer@mail.com")
    */
  }

  static void exercise1(){
    MutablePerson1 mp1 = new MutablePerson1();
    mp1.setName("Elmer");
    mp1.setEmails(Collections.singletonList("elmer@mail.com"));

    System.out.println(mp1);
    badFunctionSet(mp1);
    System.out.println(mp1);
  }

  static void exercise2(){
    MutablePerson2 mp2 = new MutablePerson2("Elmer",
            new ArrayList<>(Arrays.asList("elmer@mail.com")));

    System.out.println(mp2);
    BadFunctionGet(mp2);
    System.out.println(mp2);
  }

  static void exercise3(){
    MutablePerson3 mp3 = new MutablePerson3("Elmer",
            new ArrayList<>(Arrays.asList("elmer@mail.com")));

    System.out.println(mp3);
    BadFunctionGetFinal(mp3);
    System.out.println(mp3);
  }

  static void exercise4(){
    MutablePerson3 mp3 = new MutablePerson4("Elmer",
            new ArrayList<>(Arrays.asList("elmer@mail.com")));

    for(String email : mp3.getEmails()){
      System.out.println(email);
    }
  }

  /**
   * Este metodo modifica una propiedad y la lista mediante setter.
   * Tener el setter es peligroso…
   */
  static void badFunctionSet(MutablePerson1 person) {
    person.setName("Gabriela");
    person.setEmails(Collections.singletonList("gabriela@mail.com"));
  }

  /**
   * Este metodo toma el objeto devuelto por el getter… pero el
   * objeto es mutable, asi que podemos modificarlo sin restricciones…
   */
  static void BadFunctionGet(MutablePerson2 person) {
    List<String> emails = person.getEmails();
    emails.clear();
    emails.add("badGuy@gmail.com");
  }

  /**
   * Este metodo toma el objeto devuelto por el getter… pero el
   * objeto es mutable asi sea final, asi que podemos modificarlo sin restricciones…
   */
  static void BadFunctionGetFinal(MutablePerson3 person) {
    List<String> spammyEmails = new ArrayList<>(Arrays
            .asList("badGuy1@mail.com","badGuy2@mail.com"));
    List<String> emails = person.getEmails();
    emails.clear();
    emails.addAll(spammyEmails);
  }
}
