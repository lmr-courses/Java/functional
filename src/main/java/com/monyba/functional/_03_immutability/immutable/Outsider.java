package com.monyba.functional._03_immutability.immutable;

import java.util.ArrayList;
import java.util.List;

public class Outsider {
  public static void main(String[] args) {
    String firstName = "Elmer";
    String lastName = "Montenegro";

    List<String> emails = new ArrayList<>();
    emails.add("elmer@gmail.com");

    ImmutablePerson imper = new ImmutablePerson(firstName, lastName, emails);

    System.out.println(imper);
    badIntentionedMethod(imper);
    System.out.println(imper);
  }

  /**
   * No importa que el metodo intente modificar a la persona, la persona esta diseñada
   * para no recibir modificaciones.
   */
  static void badIntentionedMethod(ImmutablePerson person) {
    String firstName = person.getFirstName();
    firstName = "Bad";
    String lastName = person.getLastName();
    lastName = "Guy";
    List<String> emails = person.getEmails();
    emails.clear();
    emails.add("badguy@gmail.com");
  }
}
