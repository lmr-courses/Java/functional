Esta Aplicacion fue construida con Maven usando el arquetipo: maven-archetype-quickstart

Se le agrego Maven Wrapper con: mvn wrapper:wrapper

Se puede ejecutar usando: ./mvnw compile exec:java -Dexec.mainClass="com.monyba.mensajes.App" -Dexec.arguments="Hello,World"

Se agrego y configuro (exec-maven-plugin) un pluging en el POM para hacerlo mas simple usando : ./mvnw compile exec:java

Otros Comandos:  
./mvnw clean package  
./mvnw clean install  